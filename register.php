<?php require_once 'functions.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dequode - Rebooted!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="./css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="screen">
</head>
<body>
	<div id="fb-root"></div>
	<script>
	  window.fbAsyncInit = function() {
	    // init the FB JS SDK
	    FB.init({
	      appId      : '<?php echo $config["fb_app_id"]; ?>',                        // App ID from the app dashboard
	      channelUrl : '//<?php echo $config["base_url"]; ?>/channel.html', // Channel file for x-domain comms
	      cookie     : true,
	      status     : true,                                 // Check Facebook Login status
	      xfbml      : true                                  // Look for social plugins on the page
	    });

	    // Additional initialization code such as adding Event Listeners goes here
	  };

	  // Load the SDK asynchronously
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/all.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	<!--header-->
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./index.php">DEQUODE</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a data-toggle="modal" href="#leader">Leaderboard</a></li>
				<li><a data-toggle="modal" href="#rules">Rules</a></li>
			</ul>
			<form class="navbar-form navbar-right">
				<div class="fb-login-button" size="xlarge" autologoutlink="true" registration-url="<?php echo $config["base_url"]; ?>/register.php">Register/Login</div>
			</form>
		</div>
	</nav><!--/.navbar -->

	<!-- Page contents -->
	<div class="container">
		<div class="page-header">
			<h1>DEQUODE <small>The Mystery is back!</small></h1>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<fb:registration fields="[
				{'name':'name'},
				{'name':'nick','description':'Nickname','type':'text'},
				{'name':'email'},
				{'name':'college','description':'College Name','type':'typeahead','categories':['school','university']},
				{'name':'captcha'}
				]"
				redirect-uri="<?php echo $config["base_url"]; ?>/index.php"
				fb_only="true">
				</fb:registration>				
			</div>
		</div>
	</div>
	<div class="modal fade" id="leader" tabindex="-1" role="dialog" aria-labelledby="leaderlabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Leaderboard</h4>
	        </div>
	        <div class="modal-body">
	        	<?php
	          include 'leaderboard.php'; ?>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="rules" tabindex="-1" role="dialog" aria-labelledby="ruleslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Rules</h4>
	        </div>
	        <div class="modal-body">
	        	<p>1) The aim of this event is getting ahead to the next level in any way possible.<br>
	        		2) Use URL manipulation, image manipulation, viewing page source and any method you can think of!<br>
	        		3) If needed, hints will be posted by admin on facebook page.<br>
	        		4) Posting answers or direct hints on discussion page shall invite disqualification.<br>
	        		5) Answers will be in lowercase letters, no punctuation marks, no spaces.<br>
	        		6) If the answer contains a number write it in words.<br>
	        		(eg. K9 can be written as "knine" and 2012 as "twozeroonetwo".)</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<!-- Footer -->
<?php include 'footer.html'; ?>
</body>
</html>