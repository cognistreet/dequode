<?php

session_start();

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
require_once("db.php");

function login($email, $password){
	$url = 'http://cognizance.org.in/cognistreet/userAuth.php';
	$fields = array(
		'email' => urlencode($email), 
		'password' => urlencode($password)
	);
	$fields_string = "";

	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	$result = curl_exec($ch);
	// $result = "ok";
	$result = preg_replace('/\s+/', '', $result);
	curl_close($ch);
	return $result;
}

$response = "";

if(isset($_POST['email']) && isset($_POST['password'])){
	$email = $_POST['email'];
	$password = $_POST['password'];
	$response = login($email, $password);
}

if ($response != ""){

	/* Signin */
	$check = isuser_registered($response);
	if($check == true){
		$_SESSION['CID'] = $response;
		$_SESSION['EMAIL'] =  get_user_email($response)[0];
		header("Location: home.php");
	} 
	else {
		$check = add_user($email, $response, $response);
		if($check == "true"){
			$_SESSION['CID'] = $response;
			$_SESSION['EMAIL'] =  $email;
			$_SESSION['NICK_SET'] = "NO";
			header("Location: nick.php");
		}
		else {
			$_SESSION['ERROR'] = $check;
			header("Location: index.php");
		};
	}
	
} 

if(not_set_nick($_SESSION['CID']) == true){
	if(isset($_POST['nick'])){
		$nick = $_POST['nick'];

		$check = update_user_nick($_SESSION['CID'], $nick);
		if($check == "true"){
			$_SESSION["NICK"] = $nick;
			$_SESSION['NICK_SET'] = "YES";
			header("Location: home.php");
		}
		else {
			$_SESSION['ERROR'] = "Nick exits";;
			header("Location: nick.php");
		}
	}
	else {
		header("Location: nick.php");
	}
}

?>